package ar.uba.fi.tdd.exercise;
// 79979 - GONZALEZ, JUAN MANUEL
// 75.10 - 1C2021

//Class representing the GildedRose App.
//Updates the in stock items quality.
class GildedRose {

    //WARNING! We can't change this attribute! Or the Goblin, who is the owner of this code, will shoot us.
    Item[] items;

    //Constants, numeric
    //This should be in the Item class/subclasses, but we can't modify it.
    private final int MIN_QUALITY =  0;
    private final int MAX_QUALITY = 50;
    @SuppressWarnings("FieldCanBeLocal")
    private final int BACKST_DAYS =  5;
    @SuppressWarnings("FieldCanBeLocal")
    private final int INC_QUALITY =  1;
    @SuppressWarnings("FieldCanBeLocal")
    private final int INC_SELLIN  =  1;

    //Constructor, with attribute initialization.
    public GildedRose(Item[] _items) {

        items = _items;
    }

    //Checks if the item quality is between valid limits to be updated
    private boolean checkQuality(Item _currentItem) {

        return (_currentItem.quality >= MIN_QUALITY) && (_currentItem.quality <= MAX_QUALITY);
    }

    //Updates the sellIn number of days
    private void updateSellIn(Item _currentItem) {

        _currentItem.sellIn = _currentItem.sellIn - INC_SELLIN;
    }

    //Updates the quality of the item
    //Use multiplier to cover all cases:
    // - multiplier: -1 - quality decrease
    // - multiplier: -2 - quality decrease, 2x
    // - multiplier:  1 - quality increase
    // - multiplier:  2 - quality increase, 2x
    // - multiplier:  3 - quality increase, 2x
    private void updateQuality(Item _currentItem, int _multiplier) {

        //Quality value calculation
        _currentItem.quality = _currentItem.quality + (INC_QUALITY * _multiplier);

        //Over limit, assigning maximum possible value
        if (_currentItem.quality > MAX_QUALITY) {
            _currentItem.quality = MAX_QUALITY;
        }
        //Over limit, assigning minimum possible value
        if (_currentItem.quality < MIN_QUALITY) {
            _currentItem.quality = MIN_QUALITY;
        }
    }

    //Given the list of items in stock, updates the quality
    public void updateQuality() {

        for (Item currentItem : items) {

            //Original implementation uses 'equals', so exact match is used.
            switch(currentItem.Name) {
                case "Aged Brie":
                    if (checkQuality(currentItem)) {
                        updateQuality(currentItem, 1);
                    }
                    updateSellIn(currentItem);
                    break;
                case "Sulfuras, Hand of Ragnaros":
                    //Nothing has to be done in this case!
                    //This is a legendary item and does not vary in quality.
                    break;
                case "Backstage passes to a TAFKAL80ETC concert":
                    if (checkQuality(currentItem)) {
                        if ((currentItem.sellIn > 0) && (currentItem.sellIn <= BACKST_DAYS)) {
                            updateQuality(currentItem, 3);
                        } else if ((currentItem.sellIn >= (BACKST_DAYS + 1)) && (currentItem.sellIn <= (BACKST_DAYS * 2))) {
                            updateQuality(currentItem, 2);
                        } else if (currentItem.sellIn > (BACKST_DAYS * 2)) {
                            updateQuality(currentItem, 1);
                        } else {
                            currentItem.quality = MIN_QUALITY;
                        }
                    }
                    updateSellIn(currentItem);
                    break;
                case "Conjured":
                    //Conjured items, new class of special items!
                    //Quality decreases twice as fast as generic items.
                    if (checkQuality(currentItem)) {
                        if (currentItem.sellIn >= 0) {
                            updateQuality(currentItem, -2);
                        } else {
                            updateQuality(currentItem, -4);
                        }
                    }
                    updateSellIn(currentItem);
                    break;
                default:
                    //Generic item, quality processing
                    if (checkQuality(currentItem)) {
                        if (currentItem.sellIn >= 0) {
                            updateQuality(currentItem, -1);
                        } else {
                            updateQuality(currentItem, -2);
                        }
                    }
                    updateSellIn(currentItem);
            }
        }
    }
}
