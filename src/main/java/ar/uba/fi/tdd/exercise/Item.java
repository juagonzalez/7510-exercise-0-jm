package ar.uba.fi.tdd.exercise;
// 79979 - GONZALEZ, JUAN MANUEL
// 75.10 - 1C2021

//Class representing an Item sold by the Gilded Rose.
//WARNING! We can't change this class! Or the Goblin, who is the owner of this code, will shoot us.
public class Item {

    //Public attributes! Please let me fix this Mr. Goblin...
    public String Name;
    public int sellIn;
    public int quality;

    //Constructor, with attribute initialization.
    public Item(String _name, int _sellIn, int _quality) {
        this.Name = _name;
        this.sellIn = _sellIn;
        this.quality = _quality;
    }

    //toString overrode to represent an item as a string.
    @Override
    public String toString() {

        return this.Name + ", " + this.sellIn + ", " + this.quality;
    }
}
