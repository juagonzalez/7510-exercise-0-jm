package ar.uba.fi.tdd.exercise;
// 79979 - GONZALEZ, JUAN MANUEL
// 75.10 - 1C2021

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Assertions.assertThat;

//Class for automated tests:
//	- prefix 'original': tests for original functionality.
//	- prefix 'conjured': tests for 'conjured items'.
@SpringBootTest
class GildedRoseTest {

	//Tests the update of generic items.
	//Input:
	//	- 0: a generic item with positive SellIn, positive Quality.
	//  - 1: a generic item with negative SellIn, positive Quality.
	//	- 2: a generic item with positive SellIn, 0 Quality.
	//Expected output:
	//	- 0: the item quality is decreased by 1, SellIn is decreased by 1.
	//	- 1: the item quality is decreased by 2, SellIn is decreased by 1.
	//  - 2: the item quality remains at 0, SellIn is decreased by 1.
	@Test
	public void originalGenericItems() {

		Item[] items = new Item[] {
				new Item("Generic",  5, 20),
				new Item("Generic", -1, 20),
				new Item("Generic",  0,  0)
		};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(19).isEqualTo(app.items[0].quality);
		assertThat( 4).isEqualTo(app.items[0].sellIn);
		assertThat(18).isEqualTo(app.items[1].quality);
		assertThat(-2).isEqualTo(app.items[1].sellIn);
		assertThat( 0).isEqualTo(app.items[2].quality);
		assertThat(-1).isEqualTo(app.items[2].sellIn);
	}

	//Tests the update of special items.
	//Input:
	//	- 0: an 'Aged Brie' item with positive SellIn, positive Quality.
	//  - 1: an 'Aged Brie' item with negative SellIn, 50 Quality.
	//	- 2: a 'Sulfuras, Hand of Ragnaros' item with 0 SellIn, 80 Quality.
	//	- 3: a 'Backstage passes to a TAFKAL80ETC concert' item with positive SellIn, 20 Quality.
	//	- 4: a 'Backstage passes to a TAFKAL80ETC concert' item with SellIn between 6 and 10, 20 Quality.
	//	- 5: a 'Backstage passes to a TAFKAL80ETC concert' item with SellIn between 0 and 5, 20 Quality.
	//	- 6: a 'Backstage passes to a TAFKAL80ETC concert' item with SellIn of 0, 20 Quality.
	//Expected output:
	//	- 0: the item quality is increased by 1, SellIn is decreased by 1.
	//	- 1: the item quality is not increased past 50, SellIn is decreased by 1.
	//  - 2: the item quality remains at 80, SellIn remains at 0.
	//	- 3: the item quality is decreased by 1, SellIn is decreased by 1.
	//	- 4: the item quality is decreased by 2, SellIn is decreased by 1.
	//	- 5: the item quality is decreased by 3, SellIn is decreased by 1.
	//	- 6: the item quality drops to 0, SellIn is decreased by 1.
	@Test
	public void originalValidSpecialItems() {

		Item[] items = new Item[] {
				new Item("Aged Brie",  5, 20),
				new Item("Aged Brie",  5, 50),
				new Item("Sulfuras, Hand of Ragnaros", 0, 80),
				new Item("Backstage passes to a TAFKAL80ETC concert",  15,  20),
				new Item("Backstage passes to a TAFKAL80ETC concert",   9,  20),
				new Item("Backstage passes to a TAFKAL80ETC concert",   2,  20),
				new Item("Backstage passes to a TAFKAL80ETC concert",  0,  20)
		};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(21).isEqualTo(app.items[0].quality);
		assertThat( 4).isEqualTo(app.items[0].sellIn);
		assertThat(50).isEqualTo(app.items[1].quality);
		assertThat( 4).isEqualTo(app.items[1].sellIn);
		assertThat(80).isEqualTo(app.items[2].quality);
		assertThat( 0).isEqualTo(app.items[2].sellIn);
		assertThat(21).isEqualTo(app.items[3].quality);
		assertThat(14).isEqualTo(app.items[3].sellIn);
		assertThat(22).isEqualTo(app.items[4].quality);
		assertThat( 8).isEqualTo(app.items[4].sellIn);
		assertThat(23).isEqualTo(app.items[5].quality);
		assertThat( 1).isEqualTo(app.items[5].sellIn);
		assertThat( 0).isEqualTo(app.items[6].quality);
		assertThat(-1).isEqualTo(app.items[6].sellIn);

	}

	//Tests the update of invalid items.
	//Input:
	//	- 0: a generic item with positive SellIn, positive Quality.
	//  - 1: an 'Aged Brie' item with negative SellIn, 50 Quality.
	//	- 2: a 'Sulfuras, Hand of Ragnaros' item with 0 SellIn, 80 Quality.
	//	- 3: a 'Backstage passes to a TAFKAL80ETC concert' item with positive SellIn, 20 Quality.
	//Expected output:
	//	- 0: the item quality is not processed, SellIn is decreased by 1.
	//	- 1: the item quality is not processed, SellIn is decreased by 1.
	//  - 2: the item is not processed.
	//	- 3: the item quality is not processed, SellIn is decreased by 1.
	@Test
	public void originalInvalidItems() {

		Item[] items = new Item[] {
				new Item("Generic", -1, -1),
				new Item("Aged Brie",  5, 55),
				new Item("Sulfuras, Hand of Ragnaros", 10, 20),
				new Item("Backstage passes to a TAFKAL80ETC concert",  15,  90)
		};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(-1).isEqualTo(app.items[0].quality);
		assertThat( -2).isEqualTo(app.items[0].sellIn);
		assertThat(55).isEqualTo(app.items[1].quality);
		assertThat( 4).isEqualTo(app.items[1].sellIn);
		assertThat(20).isEqualTo(app.items[2].quality);
		assertThat( 10).isEqualTo(app.items[2].sellIn);
		assertThat(90).isEqualTo(app.items[3].quality);
		assertThat(14).isEqualTo(app.items[3].sellIn);
	}

	//Tests the update of valid 'conjured' items.
	//Input:
	//	- 0: a 'conjured' item with positive SellIn, positive Quality.
	//  - 1: a 'conjured' item with negative SellIn, positive Quality.
	//	- 2: a 'conjured' item with positive SellIn, and a Quality of 1.
	//Expected output:
	//	- 0: the item quality is decreased by 2, SellIn is decreased by 1.
	//	- 1: the item quality is decreased by 4, SellIn is decreased by 1.
	//  - 2: the item quality is decreases to 0, SellIn is decreased by 1.
	@Test
	public void conjuredValidItems() {

		Item[] items = new Item[] {
				new Item("Conjured", 10, 20),
				new Item("Conjured", -2, 50),
				new Item("Conjured",  5,  1)
		};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(18).isEqualTo(app.items[0].quality);
		assertThat( 9).isEqualTo(app.items[0].sellIn);
		assertThat(46).isEqualTo(app.items[1].quality);
		assertThat(-3).isEqualTo(app.items[1].sellIn);
		assertThat( 0).isEqualTo(app.items[2].quality);
		assertThat( 4).isEqualTo(app.items[2].sellIn);
	}

	//Tests the update of invalid 'conjured' items.
	//Input:
	//	- 0: a 'conjured' item with positive SellIn, positive Quality.
	//Expected output:
	//	- 0: the item quality is not processed, SellIn is decreased by 1.
	@Test
	public void conjuredInvalidItems() {

		Item[] items = new Item[]{
				new Item("Conjured", 20, 55)
		};

		GildedRose app = new GildedRose(items);
		app.updateQuality();
		assertThat(55).isEqualTo(app.items[0].quality);
		assertThat(19).isEqualTo(app.items[0].sellIn);
	}

}
